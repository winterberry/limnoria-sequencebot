from supybot import utils, plugins, ircutils, callbacks, ircmsgs
from supybot.commands import *
import datetime
import time
import threading
import json

try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('PostSequence')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


class PostSequence(callbacks.Plugin):
    """Posts a sequence of messages"""
    
    def __init__(self, irc):
        self.__parent = super(PostSequence, self)
        self.__parent.__init__(irc)

        # we set up self.irc so that calls to self.irc.reply will be sent to #hello_abc_f4ec
        msg = ircmsgs.join("#hello_abc_f4ec")
        self.irc = callbacks.ReplyIrcProxy(irc, msg)

        self.lastTimestamp = None
        self.messageCount = 0
        self.schedulePostTimestamp()
        
    def schedulePostTimestamp(self):
        self.lastTimestamp = datetime.datetime.now()
        threading.Timer(0.4, self.postTimestamp).start()
        
    def postTimestamp(self):
        timestamp = datetime.datetime.now()
        self.messageCount += 1
        message = {
            "count": self.messageCount,
            "timestamp": str(timestamp)
        }
        try:
            self.irc.reply(json.dumps(message), prefixNick=False)
        except Exception as e:
            self.log.error("Failed to send message: {}".format(str(e)))
        self.schedulePostTimestamp()


Class = PostSequence


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
